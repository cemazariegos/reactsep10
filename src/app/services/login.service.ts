import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http'
import {map} from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http:HttpClient) { }

  headers : HttpHeaders = new HttpHeaders({
    "Content-Type":"application/json"
  })
  //get users
  getUser(){
    const url = "http://localhost:1337/users";
    return this.http.get(url);
  }

  postLogin(correo:string, contrasena:string){
    const url = "http://localhost:1337/auth/local";
    
   // return this.http.post(url,{  "identifier": correo, "password": contrasena},{headers:this.headers}).pipe(map(data=>data));
   return this.http.post(url,{  "identifier": correo, "password": contrasena},{headers:this.headers});
  }

  // METODO POST PARA REGISTRAR UN NUEVO USUARIO
  postResgistro(name:string, lastname:string, username:string, numberPhone:number,
                direction:string, email:string, password:string, tipo:string){
    const url = "http://localhost:1337/auth/local/register";

    return this.http.post(url,{"nombre":name, "apellido":lastname, "username":username,
                                "email":email, "direccion":direction, "telefono":numberPhone,
                                "password":password, "tipo":tipo} );
  }

}
