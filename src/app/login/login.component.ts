import { Component, OnInit } from '@angular/core';
import { LoginService} from '../services/login.service'

import {login} from '../models/login.interface';
import { flatMap } from 'rxjs/operators';
import { CanActivate, Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(public loginService: LoginService,private router: Router) {

   }

  ngOnInit(): void {
    //la llegada de datos
  /*  this.loginService.getUser().subscribe((res:login[])=>{
      console.log("*****************************************");
      this.credenciales= res;
    
    })*/
  }

  correo:string ="";
  contrasena:string ="";
  valido:boolean=false;
  validoaux:boolean=false;


  verificar_credenciales(){
   
      console.log("Usuario:" + " "+ this.correo + " contraseña: "+ this.contrasena);

      this.loginService.postLogin(this.correo,this.contrasena).subscribe((res:login)=>{
       
      console.log("Mostrando los datos: ");
      console.log(res.user.email);
  
     
      if(res.user.email===this.correo){
        this.valido=true;
        this.correo="";
        this.contrasena="";
        
        }
      
      },
      error=>{
        this.validoaux=true;
        this.correo="";
        this.contrasena="";
      }
      
      );

      this.valido=false;
      this.validoaux=false;

    
    } 
  
   
    
   
}

