import { Component, OnInit } from '@angular/core';

import { LoginService} from '../services/login.service'
import { Registro } from '../models/registro.interface';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  // VARIABLES PARA MOSTRAR EL DIV DEL FORMULARIO PARA CADA USUARIO/PROFESIONAL
  clientStatus: boolean = false;
  workStatus: boolean = false;

  // VARIABLES PARA MOSTRAR LAS NOTIFICACIONES
  acceptNotificacion: boolean = false;
  errorNotificacion1: boolean = false;


  // VARIABLES PARA RECOLECTAR INFORMACION
  name: string = "";
  lastname: string = "";
  username: string = "";
  numberPhone: number;
  direction: string = "";
  email: string = "";
  password: string = "";

  constructor(public registro:LoginService) { }

  ngOnInit(): void {
  }


  MostrarFormularioCliente(): void{
    // MUESTRO EL FORMULARIO PARA CLIENTE
    if(!this.clientStatus)
      this.clientStatus = true;

    this.workStatus = false;
  }

  MostrarFormularioTrabador(): void{
    // LLAMO EL FORMULARIO BASE
    this.MostrarFormularioCliente(); 

    // MUESTRO EL FORMULARIO PARA EL PROFESIONAL
    if(!this.workStatus)
      this.workStatus = true;
  }


  Verificar_Datos(): void{
    let tipo=(this.clientStatus)?"cliente":"proveedor";
 
    this.registro.postResgistro(this.name,this.lastname,this.username,this.numberPhone,
      this.direction,this.email,this.password,tipo).subscribe((res:Registro)=>{

        this.acceptNotificacion = true;
        this.Limpiar_Variables();


      },
      error=>{
        this.errorNotificacion1=true;
        this.Limpiar_Variables();
      });

      this.acceptNotificacion = false;
      this.errorNotificacion1 = false;

  }

  Limpiar_Variables(): void{
    this.name = "";
    this.lastname = ""; 
    this.username = "";
    this.numberPhone = 0; 
    this.direction = ""; 
    this.email = "";
    this.password = "";
  }

}
